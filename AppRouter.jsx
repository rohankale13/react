import React from 'react';
import { Router, Route, Link, browserHistory, IndexRoute }
    from 'react-router';
import { Navbar, NavLink } from 'react-bootstrap';

class AppRouter extends React.Component {
    render() {
        return (
            <div>
                
                
                    <div className="navbar-nav">
                        <Navbar className="navbar navbar-expand-lg navbar-light bg-light">
                            <Link className="navbar-brand" to="home">Home</Link>
                            <Link className="navbar-brand" to="claim">Claim Summary</Link>
                            <Link className="navbar-brand" to="about">About Us</Link>
                            <Link className="navbar-brand" to="details">Details Us</Link>
                        </Navbar >
                        {this.props.children}
                    </div>
            </div>


        )
    }
}

export default AppRouter;




