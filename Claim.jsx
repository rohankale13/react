import React from 'react';
import axios from 'axios';
import {browserHistory } from 'react-router';
import HeaderMenu from './HeaderMenu.jsx';
import { Alert } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

class ClaimList extends React.Component {

   constructor(props){
   
     super(props);     
     this.state = {
       claims: []       
     }     
   }

   componentDidMount() {

      axios.get(`http://localhost:7001/claims`)// (1st change)
      .then(res => {
        const claims = res.data;
        this.setState({ claims });
      })
      .catch(error => {
         this.setState({claims:null})
         console.log('error', error);
      })
   }

    showUpdate(thisClaim) {
      console.log('updating claim', thisClaim.empId);
      browserHistory.push('/updateclaim/'+thisClaim.id);
      //this.setState({showUpdate: true, selectedClaim: thisClaim})
    }
  

    render() {
      
         //due to some scope issue inside the .map function, assinging the below function to a variable
         console.log('*', this.props,'**', this.props.loggedInUser);
        let showUpdateContent = this.showUpdate;
        let messageDisplay = 'messageHide';

        let disp = (this.props.params.showMessage === 'true');
        console.log('this.props.params.userId', this.props.params.userId);
        
        if(disp) {
            messageDisplay = 'messageShow';
        }
        console.log('messageDisplay', messageDisplay);

/*
"id": "AA-789999",
        "employee_name": "Jim Jhon",
        "claim_namber": "123-ABC-567",
        "claim_type": "Paid",
        "claim_program": "Maturity",
        "claim_start_date": "2020-08-01",
        "claim_end_date": "2020-08-05"
*/

       //columns and attribute mapping
       const columns = [{
         text: 'Employee ID',
         dataField: 'id'
       }, {
         text: 'Employee Name',
         dataField: 'employee_name'
       }, {
         text: 'Claim Number',
         dataField: 'claim_namber'
       },
       {
         text: 'Claim Type',
         dataField: 'claim_type'
       },
       {
         text: 'Claim Program',
         dataField: 'claim_program'
       },
       {
         text: 'Claim Start Date',
         dataField: 'claim_start_date'
       },
       {
         text: 'Claim End Date',
         dataField: 'claim_end_date'
       },
       {
         text: 'Action',
         dataField: '',
         formatter: (cellContent, row) => (            
              <label>
                 <a href='#'><img src='/assets/edit.png'alt="edit" style={{width: '25px', height: '25px'}}/></a>
              </label>            
          ),
          events: {
            onClick: (e, column, columnIndex, row, rowIndex) => {              
              
              console.log('updating claim with id:', row.id);
              console.log('rowid=',row.id)              
              browserHistory.push('/updateclaim/'+row.id);
            }
         }
       }
      ];
       
      const rowStyle2 = (row, rowIndex) => {
         const style = {};
         if (rowIndex % 2 == 0) {
           style.backgroundColor = '#d3d3d3';
           
         } else {
           style.backgroundColor = '#808080';
           style.color = 'white';
         }
         return style;
       };

        return (
           
           
         <div>
            {/* <HeaderMenu active='claimlist' userId={this.props.params.userId}/> */}
             <div className="Title">
                <label>Claim Summary</label>                  
             </div>
            
            <div className={messageDisplay} id="messageDiv" >
               <Alert key='error-message' variant='success'>The claim details are updated successfully.</Alert>
            </div>
            
             {/* boot strap table will iterate and create a table */}
             <div id='listTableDiv'>
            <BootstrapTable keyField='id' data={ this.state.claims } columns={ columns } rowStyle={ rowStyle2 } pagination={ paginationFactory() } />
            </div>
            
         </div>
        )
    }
}

export default ClaimList;