import React, { Component } from 'react';
import axios from 'axios';
import {browserHistory } from 'react-router';
import HeaderMenu from './HeaderMenu.jsx';
import {Form, Col, Row, Container, Button}  from 'react-bootstrap';

class UpdateClaim extends Component {

    constructor(props) {
        
        super(props);
        this.state = {
            showUpdate: this.props.showUpdate,
            claimTypeValues:[
                {"key":"Paid", "value":"Paid"}, 
                {"key":"Accident", "value":"Accident"}
                /*, 
                {"key":"Pending", "value":"Pending"}, 
                {"key":"More Info Required", "value":"More Info Required"}, 
                {"key":"Denied", "value":"Denied"}, 
                {"key":"Rejected", "value":"Rejected"}, 
                {"key":"Paid", "value":"Paid"}*/
            ],
            claim:[]
        }
        this.cancelUpdate = this.cancelUpdate.bind(this);
        this.submitClaim = this.submitClaim.bind(this);
    }

    componentDidMount() {
        console.log('fetching claim info for ', this.props.params.id);
        axios.get(`http://localhost:7001/claims/${this.props.params.id}`)
        .then(res => {
          const claim = res.data;
          console.log(claim);
          this.setState({ claim });
        })
        .catch(error => {
           this.setState({claim:null})
           console.log('error', error);
        })
     }

    cancelUpdate() {        
        console.log('cancelling..');
        this.setState({showUpdate: false});        
        browserHistory.push('/claim/false/admin');
    }

    submitClaim(e) {

        e.preventDefault();
        console.log('Updating claim', this.refs);
        let putJson = '';
        /*

        "id": "AA-789999",
        "employee_name": "Jim Jhon",
        "claim_namber": "123-ABC-567",
        "claim_type": "Paid",
        "claim_program": "Maturity",
        "claim_start_date": "2020-08-01",
        "claim_end_date": "2020-08-05"
        */
        let claimObj = {
            // id: this.refs['id'].value,
            id: this.refs['id'].value,
            employee_name: this.refs['employee_name'].value,
            claim_namber: this.refs['claim_namber'].value,
            claim_type: this.refs['claim_type'].value,
            claim_program: this.refs['claim_program'].value,
            claim_start_date: this.refs['claim_start_date'].value,
            claim_end_date: this.refs['claim_end_date'].value
        };
        for (const field in this.refs) {
            console.log(field);
            putJson +=  field + ':"' + this.refs[field].value + '"';            
            if(field !== 'claim_end_date') {
                putJson += ",";
            }
        }
        console.log('claimObj', claimObj);
        putJson += ''        
        console.log(putJson);
        
        axios.put('http://localhost:7001/claims/' + this.refs['id'].value, claimObj)
        .then(res => {
            console.log('res.status', res.status);            
            browserHistory.push('/claim/true/admin');            
            //4x - hashhistory
            //router.push
        });        
    }

    render() {
        console.log('this.props.id', this.state.claim);
        console.log('From Store: this.props.claimObj', this.props.claimObj);
        if(this.state.claim) {
            const {id,  employee_name, claim_namber, claim_type, claim_program, claim_start_date, claim_end_date} = this.state.claim;
            return (
                <div>
                <HeaderMenu active='claimupdate'/>
                <Container className="align-items-center">
                    <Form onSubmit={this.submitClaim}>  
                    
                    <Row className="justify-content-md-center">
                        <Col >
                            {/* <input type="hidden" defaultValue={id}  name="id" ref="id"  /> */}
                            <input type="hidden" defaultValue={id}  name="id" ref="id" />
                            
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                        <Form.Group>
                            <Form.Label>Employee Name</Form.Label>
                            <Form.Control type="text"  defaultValue={employee_name} disabled  name="employee_name" ref="employee_name"/>
                        </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                        <Form.Group>
                            <Form.Label>Claim Number</Form.Label>
                            <Form.Control type="text" defaultValue={claim_namber}  name="claim_namber" ref="claim_namber"
                                        pattern="[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}" maxLength="11"/>
                        </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                        <Form.Group>
                            <Form.Label>Claim Type</Form.Label>
                            <Form.Control as="select" defaultValue={claim_type} ref="claim_type">
                            {
                                    this.state.claim_typeValues.map(claim_type => {
                                        
                                        return (<option value={claim_type.key} key={claim_type.key} >{claim_type.value}</option>)
                                    })
                            }
                            
                            </Form.Control>
                        </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                        <Form.Group>
                            <Form.Label>Claim Programs</Form.Label>
                            <Form.Control type="text" defaultValue={claim_program}  name="claim_program" ref="claim_program" />
                            
                        </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                        <Form.Group>
                            <Form.Label>Claim Start Date</Form.Label>
                            <Form.Control type="date" defaultValue={claim_start_date} ref="claim_start_date" />
                        
                        </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                        <Form.Group>
                            <Form.Label>Claim End Date</Form.Label>
                            <Form.Control type="date" defaultValue={claim_end_date} ref="claim_end_date"/>
                        </Form.Group>
                        </Col>

                    </Row>
                    <Row className="mx-auto">
                        <Col >
                        
                        <Button md={{ span: 3, offset: 3 }} variant="secondary" size="sm" active onClick={this.cancelUpdate}>
                            Cancel
                        </Button>
                            {' '}
                        <Button md={{ span: 3, offset: 3 }} variant="primary" size="sm" type="submit" active>
                        Update
                        </Button>
                        {' '}
                        {' '}
                        </Col>
                    </Row>
                    </Form>
                </Container>
                </div>
            )
        }
        return null;
    }
}

  
export default UpdateClaim;







// import React, { Component } from 'react'
// import { Modal, Button } from 'react-bootstrap';

// class ClaimUpdate extends Component {
//   render() {
//     return (
//       <div><Modal.Dialog>
//       <Modal.Header closeButton>
//         <Modal.Title>Update Claim</Modal.Title>
//       </Modal.Header>
    
//       <Modal.Body>
//         <p>Modal body text goes here.</p>
//       </Modal.Body>
    
//       <Modal.Footer>
//         <Button variant="secondary">Close</Button>
//         <Button variant="primary">Save changes</Button>
//       </Modal.Footer>
//     </Modal.Dialog></div>
//     )
//   }
// }

// export default ClaimUpdate;
