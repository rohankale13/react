import React from 'react';
import {browserHistory} from 'react-router';
import {Navbar, Nav, Link} from 'react-bootstrap';


class HeaderMenu extends React.Component {

    constructor() {
        //super(props);
        super();
        this.logout = this.logout.bind(this);
        
    }

    logout() {

        
        browserHistory.push('/');
    }

    logoutAction(){
        return {
            type: 'LOGIN',
            loggedInUser: null
        }
    }

    render() {
        let claimlistClass = null;
        let claimupdateClass = null;
        if(this.props.active==='claimlist') {
            claimlistClass = true;
        } else if(this.props.active==='claimupdate') {

            claimupdateClass = true;
            
        }
        console.log('this.props.active', this.props.active);
        console.log('Header: this.props.userId', this.props.userId);
        return (
            <div>
                <header>
                <div >
                    <div>
                        Claims Management
                        <div className='divHeader'>
                            Welcome <br/>{this.props.userId}
                        </div>
                    </div>
                </div>
                </header>

                { <Navbar style={{backgroundColor:"#000000"}} variant="dark">                    
                    <Nav className="mr-auto">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#claim" active={claimlistClass}>Claim Summary</Nav.Link>
                        <Nav.Link href="#claimupdate" active={claimupdateClass}>Update Claim</Nav.Link>
                    </Nav>
                    <Nav>                    
                    <Nav.Link eventKey={2} href="#" onClick={this.logout}>
                        Logout
                    </Nav.Link>
                </Nav>    
                </Navbar> }

               
                
            </div>
        )
    }
    
}


export default HeaderMenu;
