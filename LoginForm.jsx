import React from 'react';
import axios from 'axios';


class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    
  componentDidMount() {  // its Component Life Cycle MEthod Of React
    axios.get(`http://localhost:7000/users`)
      .then(res => {
        const data = res.data;
        this.setState({ data });
      })
      .catch(error => {
        this.setState({data:null})
        console.log('error', error);
     })
  }

    handleSubmit(e) {
        e.preventDefault();
        
        const formData = {};
        console.log('refs',this.refs);
        for (const field in this.refs) {
          
          formData[field] = this.refs[field].value;
        }
       
        
        let validUser = this.state.data.find(user=>user.email===formData.email);
        if  (validUser) alert('user is valid')
        else alert('user is not valid');
                
        
        
    }
        render() {
            return (
                <div>
                  <form onSubmit={this.handleSubmit}>
                    
                    <input ref="email" className="email" type='email' name="email" required/>
                    <input ref="password" className="password" type='password' name="password" required/>
                    <input type="submit" value="Submit"/>
                  </form>
                </div>
            );

}
}

export default LoginForm;