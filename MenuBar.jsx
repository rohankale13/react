import React, { Component } from 'react';
import { Navbar, Container } from 'react-bootstrap';

class MenuBar extends Component {
    render() {
        return (
            <div>
                <Navbar>
                    <Container>
                        <Navbar.Brand href="#home" className="justify-content-centre">Claim Management</Navbar.Brand>
                        <Navbar.Toggle />
                        <Navbar.Collapse className="justify-content-end">
                            <Navbar.Text>
                                {/* Signed in as: <a href="#login">Mark Otto</a> */}
                                Welcome User 
                            </Navbar.Text>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>


            </div>


        );
    }
}

export default MenuBar;